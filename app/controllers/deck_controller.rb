require 'file_module'

class DeckController < ApplicationController
  include FileModule

  def obtain_card
    return raise 'There is no cards in deck' if Deck.all.blank?
    @card = CardReceiver.receive_random_card
    save_card_json(@card)

    render json: @card
    CardReceiver.new(@card).delete
  end

  def restore_cards
    contents = read_cards_json
    return raise 'There is no cards handed out' if contents.empty?
    cards = parse_cards(contents)
    @record = CardReturner.new(cards).checkvalidity
    if @record
      @record.save
    else
      render json: {
        error: 'Card is already in the application state',
        status: 422
      }, status: 422
    end
    clear_file
  end
end
