class Deck < ActiveRecord::Base
  validates :card, uniqueness: { scope: :suit }
end
