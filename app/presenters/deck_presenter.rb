class DeckPresenter < Presenter
  def as_json(*)
    { suit: @object.suit, card: @object.card }
  end
end