class DeckSerializer < ActiveModel::Serializer
  attributes :suit, :card
end
