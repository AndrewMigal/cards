require 'random/online'

class CardReceiver
  def initialize(card)
    @card = card
  end

  def self.generator
    RealRand::RandomOrg.new
  end

  def self.receive_random_card
    return Deck.limit(1) if Deck.count == 1
    Deck.limit(1).offset((generator.randnum(1, 0, Deck.count - 1)[0]).to_i)[0]
  end

  def delete
    Deck.delete(@card)
  end
end
