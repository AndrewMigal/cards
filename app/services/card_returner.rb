class CardReturner
  attr_reader :contents

  def initialize(cards)
    @cards = cards
  end

  def checkvalidity
    @cards.each do |card|
      @record = Deck.new(card)
      return false if @record.invalid?
      return @record
    end
  end
end
