class Shuffler
  def deck_arr
    Deck.all.to_a.map! do |card|
      { suit: card.suit, card: card.card }
    end
  end

  def self.shuffle
    deck_array = deck_arr
    (Deck.count - 1).downto(1) do |i|
      j = rand(0..i)
      deck_array[i], deck_array[j] = deck_array[j], deck_array[i]
    end
    insert_shuffled(deck_array)
  end

  def insert_shuffled(deck_arr)
    Deck.delete_all
    Deck.create!(deck_arr)
  end
end
