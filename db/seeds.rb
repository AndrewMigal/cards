suit = %w(spade heart club diamond)
card = (2..13).to_a.map(&:to_s).push('a')
suit.each do |s|
  card.each do |c|
    Deck.create!(suit: s, card: c)
  end
end
Deck.create!(card: 'joker')
