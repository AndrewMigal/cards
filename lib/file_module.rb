module FileModule
  def save_card_json(card)
    File.open('bin/card.json', 'a') do |f|
      f.write(', ') unless File.zero?(f)
      f.write(card.to_json)
    end
  end

  def fname
    'bin/card.json'
  end

  def read_cards_json
    File.read(fname)
  end

  def clear_file
    File.truncate(fname, 0)
  end

  def parse_cards(contents)
    contents.split(', ').map! { |card| JSON.parse(card) }
  end
end
