require 'rails_helper'

RSpec.describe DeckController, type: :controller do
  describe 'GET #obtain_card' do
    context 'when there are no cards' do
      it 'raises error' do
        expect { get :obtain_card }.to raise_error(RuntimeError)
      end
    end

    context 'when there is 1 card in the deck' do
      it 'returns json with that card' do
        @card = create :deck
        @expected = {
          suit: 'spade',
          card: 'a'
        }.to_json
        get 'obtain_card', format:  :json

        expect(response.body).to include(@expected)
      end

      it 'changes card count in the deck' do
        create :deck

        expect { get :obtain_card }.to change { Deck.count }.by(-1)
      end
    end
  end

  describe 'PUT #obtain_cards_json' do
    let(:content) { { suit: 'spade', card: 'a' }.to_json }
    context 'when there are no handed out cards' do
      it 'raises error' do
        allow(controller).to receive(:read_cards_json).and_return('')

        expect { put :restore_cards }.to raise_error(RuntimeError)
      end
    end

    context 'when there are handed out cards' do
      it 'changes card count in the deck' do
        allow(controller).to receive(:read_cards_json).and_return(content)

        expect { put :restore_cards }.to change { Deck.count }.by(1)
      end
    end

    context 'when card is violating uniq validation' do
      it 'renders status 422' do
        create :deck
        allow(controller).to receive(:read_cards_json).and_return(content)
        put :restore_cards

        expect(response.status).to eq(422)
      end
    end
  end
end
